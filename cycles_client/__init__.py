import requests


class CycleNotFound(Exception):
    pass


class Cycles:

    CYCLES = 'https://cycles.tompaton.com/'

    def __init__(self, api_token):
        self._cookies = None
        self._api_token = api_token

    @classmethod
    def _url(cls, *path):
        return '{}{}'.format(cls.CYCLES, '/'.join(path))

    def _get(self, url):
        return self._send_request('GET', self._url('api', url),
                                  cookies=self._cookies).json()

    def _post(self, url, **data):
        return self._send_request("POST", self._url('api', url),
                                  json=data,
                                  cookies=self._cookies).json()

    def login(self):
        self._cookies = self._send_request('GET', self.get_login_url(),
                                           allow_redirects=False).cookies

    def get_ical_url(self):
        return self._url('ical', self._api_token)

    def get_login_url(self):
        return self._url('login', self._api_token)

    def get_cycle(self, words):
        cycles = [cycle for cycle in self._get('cycle/')['cycles']
                  if self._matches(cycle, words)]

        if len(cycles) == 1:
            return self._cycle(cycles[0])

        elif not cycles:
            raise CycleNotFound("Sorry, can't see which cycle you mean.")

        else:
            raise CycleNotFound("Sorry, can't see which cycle you mean, "
                                "it could be:\n{}"
                                .format('\n'.join(['{title} - {list_title}'
                                                   .format(**cycle)
                                                   for cycle in cycles])))

    @staticmethod
    def _matches(cycle, words):
        title = cycle['title'].lower().split()
        return all(any(kw.startswith(word.lower()) for kw in title)
                   for word in words)

    @staticmethod
    def _cycle(cycle):
        cycle['overdue_label'] = (' Overdue' if cycle['lengths']['overdue']
                                  else '')

        cycle['due_today'] \
            = cycle['lengths']['upcoming'] == 0 and not cycle['paused']

        cycle['ticked_today'] \
            = cycle['lengths']['current'] == 0

        return cycle

    def get_lists(self):
        return self._get('list/')['lists']

    def get_list_cycles(self, list_name):
        cycles = self._get('list/{}/cycle/'.format('-'.join(list_name)))
        return cycles['cycles']

    def get_cycles_todo(self, when=None):
        # TODO: support when kwarg
        return self._select_cycles('due_today')

    def get_cycles_done(self, when=None):
        # TODO: support when kwarg
        return self._select_cycles('ticked_today')

    def _select_cycles(self, include):
        cycles = self._get('cycle/')['cycles']

        for cycle in cycles:
            self._cycle(cycle)

        return [cycle for cycle in cycles if cycle[include]]

    def tick_cycle(self, name, when):
        cycle = self.get_cycle(name)
        action = self._post('cycle/{}/ticks/'.format(cycle['slug']),
                            time=when.isoformat()[:19])

        return cycle, action

    def schedule_cycle(self, name, when):
        cycle = self.get_cycle(name)
        action = self._post('cycle/{}/schedule/'.format(cycle['slug']),
                            time=when.isoformat()[:19])

        return cycle, action

    @classmethod
    def _send_request(cls, *args, **kwargs):
        return requests.request(*args, **kwargs)
