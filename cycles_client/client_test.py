from datetime import datetime

from unittest.mock import Mock, patch, sentinel, call

import pytest

import cycles_client as client


@pytest.fixture
def cycles():
    cycles = client.Cycles('secret-token')
    cycles._cookies = sentinel.cookies
    return cycles


def test_login():
    cycles = client.Cycles('secret-token')
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.cookies = sentinel.cookies

        assert cycles.login() is None

        assert cycles._cookies == sentinel.cookies

        send.assert_called_with(
            'GET', 'https://cycles.tompaton.com/login/secret-token',
            allow_redirects=False)


def test_url():
    assert client.Cycles._url('abc', 'def') \
        == 'https://cycles.tompaton.com/abc/def'


def test_get(cycles):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value = sentinel.json

        assert cycles._get('abc/def') == sentinel.json

        send.assert_called_with('GET',
                                'https://cycles.tompaton.com/api/abc/def',
                                cookies=sentinel.cookies)


def test_post(cycles):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value = sentinel.json

        assert cycles._post('abc/def', key='value') == sentinel.json

        send.assert_called_with('POST',
                                'https://cycles.tompaton.com/api/abc/def',
                                json={'key': 'value'},
                                cookies=sentinel.cookies)


def test_get_ical_url(cycles):
    assert cycles.get_ical_url() \
        == 'https://cycles.tompaton.com/ical/secret-token'


def test_get_login_url(cycles):
    assert cycles.get_login_url() \
        == 'https://cycles.tompaton.com/login/secret-token'


@pytest.mark.parametrize('title,words', [
    ('', ''),
    ('lorum', ''),
    ('lorum', 'lorum'),
    ('lorum', 'lo'),
    ('LORUM', 'lo'),
    ('lorum', 'Lo'),
    ('lorum ipsum', 'lorum'),
    ('lorum ipsum', 'ipsum'),
    ('lorum ipsum', 'lorum ipsum'),
    ('lorum ipsum', 'ipsum lorum'),
    ('lorum ipsum', 'lo'),
    ('lorum ipsum', 'ip'),
    ('lorum ipsum', 'lo ip'),
    ('lorum ipsum', 'ip lo'),
    ('lorum ipsum', 'i ip ipsum'),
])
def test_matches(title, words):
    assert client.Cycles._matches({'title': title}, words.split())


@pytest.mark.parametrize('title,words', [
    ('lorum', 'ipsum'),
    ('lorum', 'orum'),
    ('lorum ipsum', 'um'),
    ('lorum ipsum', 'ipsum dolor'),
    ('lorum ipsum', 'lorum ipsum dolor'),
    ('lorum ipsum', 'lorum ipsum d'),
])
def test_not_matches(title, words):
    assert not client.Cycles._matches({'title': title}, words)


@pytest.mark.parametrize('lengths,paused,results', [
    ((0, 0, 0), False, ('', True, True)),
    ((0, 0, 1), False, ('', True, False)),
    ((0, 1, 0), False, ('', False, True)),
    ((1, 0, 0), False, (' Overdue', True, True)),
    ((0, 0, 0), True, ('', False, True)),
])
def test_cycle(lengths, paused, results):
    length_overdue, length_upcoming, length_current = lengths
    overdue_label, due_today, ticked_today = results
    cycle = {'lengths': {'overdue': length_overdue,
                         'current': length_current,
                         'upcoming': length_upcoming},
             'paused': paused}
    expected = dict(cycle,
                    overdue_label=overdue_label,
                    due_today=due_today,
                    ticked_today=ticked_today)
    assert client.Cycles._cycle(cycle) == expected
    assert cycle == expected


def test_get_cycle_match(cycles, cycles_list):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value = {'cycles': cycles_list}

        assert cycles.get_cycle(['cycle', 'a']) == {
            'title': 'Cycle A',
            'slug': 'one-two',
            'length_label': '',
            'paused': False,
            'lengths': {'upcoming': 0, 'overdue': 0, 'current': 1},
            'due_today': True,
            'ticked_today': False,
            'overdue_label': ''}

        send.assert_called_with(
            'GET', 'https://cycles.tompaton.com/api/cycle/',
            cookies=sentinel.cookies)


def test_get_cycle_no_match(cycles, cycles_list):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value = {'cycles': cycles_list}

        with pytest.raises(client.CycleNotFound) as execinfo:
            cycles.get_cycle(['cycle', 'z'])

        send.assert_called_with(
            'GET', 'https://cycles.tompaton.com/api/cycle/',
            cookies=sentinel.cookies)

        assert str(execinfo.value) == "Sorry, can't see which cycle you mean."


def test_get_cycle_many_match(cycles, cycles_list):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value = {'cycles': cycles_list}

        with pytest.raises(client.CycleNotFound) as execinfo:
            cycles.get_cycle(['cycle', 'lorum'])

        send.assert_called_with(
            'GET', 'https://cycles.tompaton.com/api/cycle/',
            cookies=sentinel.cookies)

        assert str(execinfo.value) \
            == ("Sorry, can't see which cycle you mean, it could be:\n"
                'Cycle X lorum - Lorum Ipsum\n'
                'Cycle Y lorum - Lorum Ipsum')


def test_get_lists(cycles):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value \
            = {'lists': [{'slug': 'one', 'title': 'List One'},
                         {'slug': 'two', 'title': 'List Two'}]}

        assert cycles.get_lists() == [{'slug': 'one', 'title': 'List One'},
                                      {'slug': 'two', 'title': 'List Two'}]

        send.assert_called_with('GET', 'https://cycles.tompaton.com/api/list/',
                                cookies=sentinel.cookies)


def test_get_list_cycles(cycles):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value \
            = {'cycles': [{'slug': 'cycle-a',
                           'title': 'Cycle A',
                           'list_slug': 'list-one',
                           'list_title': 'List One'},
                          {'slug': 'cycle-b',
                           'title': 'Cycle B',
                           'list_slug': 'list-one',
                           'list_title': 'List One'}]}

        assert cycles.get_list_cycles(['list', 'one']) \
            == [{'slug': 'cycle-a',
                 'title': 'Cycle A',
                 'list_slug': 'list-one',
                 'list_title': 'List One'},
                {'slug': 'cycle-b',
                 'title': 'Cycle B',
                 'list_slug': 'list-one',
                 'list_title': 'List One'}]

        send.assert_called_with(
            'GET', 'https://cycles.tompaton.com/api/list/list-one/cycle/',
            cookies=sentinel.cookies)


@pytest.fixture
def cycles_list():
    return [
        # done
        {'title': 'Cycle X lorum',
         'list_title': 'Lorum Ipsum',
         'length_label': '',
         'paused': False,
         'lengths': {'upcoming': 2, 'overdue': 0, 'current': 0}},
        {'title': 'Cycle Y lorum',
         'list_title': 'Lorum Ipsum',
         'length_label': '',
         'paused': False,
         'lengths': {'upcoming': 3, 'overdue': 0, 'current': 0}},
        # due
        {'title': 'Cycle A',
         'slug': 'one-two',
         'length_label': '',
         'paused': False,
         'lengths': {'upcoming': 0, 'overdue': 0, 'current': 1}},
        {'title': 'Cycle B',
         'length_label': '/7d',
         'paused': False,
         'lengths': {'upcoming': 0, 'overdue': 0, 'current': 2}},
        # paused
        {'title': 'Cycle C',
         'length_label': '/28w',
         'paused': True,
         'lengths': {'upcoming': 0, 'overdue': 0, 'current': 123}},
        # not due
        {'title': 'Cycle D',
         'length_label': '',
         'paused': False,
         'lengths': {'upcoming': 1, 'overdue': 0, 'current': 3}},
        # overdue
        {'title': 'Cycle E',
         'length_label': '/3w',
         'paused': False,
         'lengths': {'upcoming': 0, 'overdue': 2, 'current': 4}},
    ]


def test_get_cycles_todo(cycles, cycles_list):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value = {'cycles': cycles_list}

        assert cycles.get_cycles_todo() == [
            {'title': 'Cycle A',
             'slug': 'one-two',
             'length_label': '',
             'paused': False,
             'lengths': {'upcoming': 0, 'overdue': 0, 'current': 1},
             'due_today': True,
             'ticked_today': False,
             'overdue_label': ''},
            {'title': 'Cycle B',
             'length_label': '/7d',
             'paused': False,
             'lengths': {'upcoming': 0, 'overdue': 0, 'current': 2},
             'due_today': True,
             'ticked_today': False,
             'overdue_label': ''},
            {'title': 'Cycle E',
             'length_label': '/3w',
             'paused': False,
             'lengths': {'upcoming': 0, 'overdue': 2, 'current': 4},
             'due_today': True,
             'ticked_today': False,
             'overdue_label': ' Overdue'},
        ]

        send.assert_called_with(
            'GET', 'https://cycles.tompaton.com/api/cycle/',
            cookies=sentinel.cookies)


def test_get_cycles_done(cycles, cycles_list):
    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.return_value = {'cycles': cycles_list}

        assert cycles.get_cycles_done() == [
            {'title': 'Cycle X lorum',
             'list_title': 'Lorum Ipsum',
             'length_label': '',
             'paused': False,
             'lengths': {'upcoming': 2, 'overdue': 0, 'current': 0},
             'due_today': False,
             'ticked_today': True,
             'overdue_label': ''},
            {'title': 'Cycle Y lorum',
             'list_title': 'Lorum Ipsum',
             'length_label': '',
             'paused': False,
             'lengths': {'upcoming': 3, 'overdue': 0, 'current': 0},
             'due_today': False,
             'ticked_today': True,
             'overdue_label': ''},
        ]

        send.assert_called_with(
            'GET', 'https://cycles.tompaton.com/api/cycle/',
            cookies=sentinel.cookies)


def test_tick_cycle(cycles, cycles_list):
    action = {'action': 'Ticked...'}

    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.side_effect = [{'cycles': cycles_list}, action]

        assert cycles.tick_cycle(
            ['cycle', 'a'], when=datetime(2017, 8, 30, 9, 0, 0)) \
            == (cycles_list[2], action)

        assert send.mock_calls == [
            call('GET', 'https://cycles.tompaton.com/api/cycle/',
                 cookies=sentinel.cookies),
            call().json(),
            call('POST', 'https://cycles.tompaton.com/api/cycle/one-two/ticks/',
                 json={'time': '2017-08-30T09:00:00'},
                 cookies=sentinel.cookies),
            call().json(),
        ]


def test_schedule_cycle(cycles, cycles_list):
    action = {'action': 'Scheduled...'}

    with patch.object(cycles, '_send_request') as send:
        send.return_value = Mock()
        send.return_value.json.side_effect = [{'cycles': cycles_list}, action]

        assert cycles.schedule_cycle(
            ['cycle', 'a'], when=datetime(2017, 8, 30, 9, 0, 0)) \
            == (cycles_list[2], action)

        assert send.mock_calls == [
            call('GET', 'https://cycles.tompaton.com/api/cycle/',
                 cookies=sentinel.cookies),
            call().json(),
            call('POST',
                 'https://cycles.tompaton.com/api/cycle/one-two/schedule/',
                 json={'time': '2017-08-30T09:00:00'},
                 cookies=sentinel.cookies),
            call().json(),
        ]
